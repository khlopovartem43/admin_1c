import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import LoginPage from '@pages/LoginPage.vue'
import { useAuth } from '@store/auth.store.ts'
import MainPage from '@pages/MainPage.vue'
import TestPage from '@pages/TestPage.vue'

const routes: RouteRecordRaw[] = [
  {
    name: 'Login',
    component: LoginPage,
    path: '/login',
    meta: {
      isRequiredLogin: false,
      isNeedNavigation: false,
    },
  },
  {
    name: 'Main',
    component: MainPage,
    path: '/',
    meta: {
      isRequiredLogin: true,
      isNeedNavigation: true,
    },
  },
  {
    name: 'Test',
    component: TestPage,
    path: '/test',
    meta: {
      isRequiredLogin: true,
      isNeedNavigation: true,
    },
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
})
router.beforeEach((to, _, next) => {
  if (!to.meta.isRequiredLogin) {
    return next()
  }

  const { login, password } = useAuth()

  if (login && password) {
    return next()
  }

  next('/login')
})
export default router
