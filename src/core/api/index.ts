import axios from 'axios'
import { useAuth } from '@store/auth.store'
import { Buffer } from 'buffer'
import { FilterDto, FilterDtoWithoutPage } from '@models/filter-dto.ts'
import { ValueItem } from '@models/value-item.ts'

export const baseURL = 'http://193.17.92.67'

export const axiosInstance = axios.create({
  baseURL: baseURL,
})

axiosInstance.interceptors.request.use((config) => {
  const { login, password } = useAuth()

  const token = Buffer.from(login + ':' + password, 'utf-8').toString('base64')

  config.headers.setAuthorization('Basic ' + token)

  return config
})

export const getRows = () => {
  return axiosInstance.post('center/hs/Obmen/v9/fillheaders/GetInfo')
}

export const filter = <T>(dto: FilterDto & T) => {
  return axiosInstance.post('center/hs/Obmen/v9/calculate/GetInfo', {
    ...dto,
  })
}

export const filterPages = (dto: FilterDtoWithoutPage) => {
  return axiosInstance.post('center/hs/Obmen/v9/numberofpages/GetInfo', {
    ...dto,
  })
}

export const getFormData = () => {
  return axiosInstance.post('center/hs/Obmen/v9/fillfilter/GetInfo')
}

export const calc = (dto: FilterDto) => {
  return axiosInstance.post('center/hs/Obmen/v9/calculate/GetInfo', {
    ...dto,
  })
}

export const save = (dto: ValueItem[]) => {
  return axiosInstance.post('center/hs/Obmen/v9/ready/GetInfo', dto)
}

export const getItems = (dto: ValueItem[]) => {
  return axiosInstance.post('center/hs/Obmen/v9/ready/GetInfo', dto)
}
