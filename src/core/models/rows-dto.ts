export interface RowsDto {
  name: string
  type: 'string'
  canEdit: boolean
}
