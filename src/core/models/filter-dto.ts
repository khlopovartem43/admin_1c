export interface FilterDto {
  page: number
  name: string
}

export type FilterDtoWithoutPage = Omit<FilterDto, 'page'>
