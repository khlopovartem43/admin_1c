export type FormDataDto = Record<string, number | boolean>
