export type ValueItem = Record<string, string | number>

export type ValueItemChecked = ValueItem & { isChecked: boolean }
export const valueItemToString = (item: ValueItem) => {
  let i = ''
  for (const data in item) {
    i += item[data]
  }
  return i
}
