export interface InputItem {
  type: 'string' | 'boolean'
  name: string
}
