import { defineStore } from 'pinia'

export interface IAuthData {
  login?: string
  password?: string
}
export const useAuth = defineStore({
  id: 'auth',
  state: (): IAuthData => {
    return {
      login: undefined,
      password: undefined,
    }
  },
  actions: {
    changeToken(data: IAuthData) {
      this.login = data.login
      this.password = data.password
    },
    reset() {
      this.login = undefined
      this.password = undefined
    },
    refresh() {},
  },
  persist: true,
})
